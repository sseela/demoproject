package com.example.demo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BuilderModel {
	private String name;
	private String address;
}
