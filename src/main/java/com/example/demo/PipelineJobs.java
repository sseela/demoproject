package com.example.demo;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(value = "PipelineJobs")
public class PipelineJobs {
	
	private String pipelineId;
	private String pipelineName;
	private List<Object> rules;
}
