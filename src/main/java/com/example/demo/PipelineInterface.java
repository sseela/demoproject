package com.example.demo;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PipelineInterface extends MongoRepository<PipelineJobs, String>{
	
	public PipelineJobs findByPipelineId(String pipelineId);
}
