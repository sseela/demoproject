package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@Autowired
	private BookRepository repo;
	
	@RequestMapping(value = "/hello")
	public String hello() {
		return "hello";
	}
	
	@PostMapping(value = "/save")
	public String saveDataToDB() {
		BookDetails book = new BookDetails();
		book.setName("wonderla");
		repo.save(book);
		return "hello";
	}
}
